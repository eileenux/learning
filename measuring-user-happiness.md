# Measuring User Happiness

*Notes from the **Measuring User Happiness 4-week Telegram plan** by Tomer Sharon*

# Why measure happiness?

- Measuring happiness helps you identify the strong and weak areas of your product.
- Tracking happiness scores over time can help you understand the effect of a change or redesign you implemented.



# Principles

Principles for successfully measuring user and customer happiness:

1. **Measure important things.** Agree with your team and leadership on what’s important. Not every small product, feature, or friction of a service is worth measuring. 
2. **Create a quantitative metric that provides a signal.** Do not try to answer every question you have about the experience. At some point, you will have to migrate to a more qualitative approach to understand why the score is what it is.
3. **Pick the right time, right place, and right people.** Measure happiness in context to get the most valid and reliable results. Otherwise, you are at risk of making decisions based on rotten data, fake news, and alternative truths. 
4. **Don’t nag.** Higher frequency of asking for happiness rating will not get you more responses. It will actually achieve the exact opposite result. People will stop responding and even become annoyed. You’ll be damaging the experience you are trying to measure and improve. More frequent surveying + too many questions  →  Lower response rates  →  Less trustworthy data + decreased ability to segment into smaller cohorts
5. **Happiness is not everything.** Remember that what people say is not necessarily consistent with their actions. People’s attitudes are interesting, but behavior is much more telling. Give more attention and weight to behavioral rather than attitudinal metrics, such as happiness and satisfaction.
6. **What won’t be simple, simply won’t be.** Don’t overdo or overcomplicate things. Succinct, direct questions with very few answer options that require minimum effort on behalf of respondents will take you a long way. 
7. **Reach a shared understanding with your team.** Agreement within the team and leadership about a certain metric is critical. Disagreement results in ignoring data and not taking action. Make sure to create a shared understanding and acceptance of what’s being measured.



# Common pitfalls

| **Mistake**                                                  | **Solution**                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Measuring overall happiness**<br />Net Promoter Score (NPS) measures entire products, which is not necessarily useful or actionable. | **Measure core & specific features**<br />Scores must be feature-specific in order to be useful in making the product better. |
| **Measuring everything**<br />Measuring everything = measuring nothing. | **Limit scope**<br />Reach an agreement on what is truly central to the experience and measure that.<br />**Start small**<br />Choose 3 features & only measure them.<br /> |
| **Long surveys**<br />Asking people to explain satisfaction ratings in writing (*why* they gave a rating) is less effective than having open-ended conversations with customers. | Make surveys short, fun & easy to respond to.<br />Supplement surveys with qualitative research to understand *why* the score is what it is. |
| **Huge rating scales**<br />TK                               | **Simplify rating scales** <br />Use thumbs up/down, or a 3-point scale. |



# Happiness metrics

1. **Satisfaction Score (happiness average).** Caclculated by averaging all happiness ratings.
2. **Satisfied Users (% of happy users).** Quantifies % of users who indicated they were happy, satisfied, or delighted with a feature, product, or service. Example: Out of 100 users, 17 are unhappy, 24 undecided, 59 happy. The score would be 59%.
3. **aNPS (promoters of your product).** "In the past week, have you recommended [product/service] to anyone?" [Yes/No] Score is measured by dividing the number of yes responses to get a percentage score.



# Taking action

## Set goals

Challenge your teams to get better scores for user happiness by setting goals. This moves teams from **delivering outputs** to **achieving outcomes**. 

### Examples

- **Set goals for different geographies.** People coming from different cultures tend to complain more and be less satisfied compared to other “happier” cultures.
- **Set goals for products in different stages.** For example, satisfaction goals for **mature products** might be set at 90%, while **new products** in their first six months after launch might be set to 70%.
- **Set a KR (Objectives and Key Results).** “At least 80% satisfaction with onboarding among new users in Asia during Q3.”

## Follow up on feedback

- When a user or customer bothers to tell you they are unhappy, that’s meaningful. They are raising a red flag and expect you to do something about it. If you don’t, they probably won’t be your customer much longer.
- Configure a survey system so that each time a customer indicates they’re unhappy, an automated email is generated from someone senior in the company to the dissatisfied customer inviting them to a conversation.
- Taking action on user feedback gives you a rare opportunity to show customers you care, and can help  increase happiness and retention.

## Evaluating the success of changes

When you look at happiness data for evaluating a redesign, things to keep in mind:

- People don’t like change and won’t be happy about it at first.
- Prior to releasing a change, take measures to ease users into it. Actions you can take to prevent and reduce the effects of change aversion:
  - Warn users about upcoming major changes in advance.
  - Clearly communicate the nature and value of the changes.
  - Let users toggle between old and new versions.
  - Provide transition instructions and support.
  - Offer users a dedicated feedback channel.
  - Tell users how you’re addressing key issues they’ve raised.
- Pay attention to happiness scores over time. If you handle the redesign well, you will also see how fast happiness scores go back to their previous levels and even higher.
- Look for how fast the data goes back up and hopefully is even higher than before. “Recovery” time is important.

**Further reading:** [Change aversion: why users hate what you launched (and what to do about it)](https://library.gv.com/change-aversion-why-users-hate-what-you-launched-and-what-to-do-about-it-2fb94ce65766)



# Measurement tips

## Start small

- “Let’s measure everything and let the data tell us what’s happening” is ineffective and wasteful.
- Identify the core, most valuable, revenue-driving, or satisfaction-driving features and track happiness. Focusing your attention on just a few critical features helps everyone understand what the experience is and where most efforts should be directed.

### Examples

- **App:** Measure one feature for a week. 
- **Gym:** Measure one class with one instructor and 10 clients. 
- **Online conference:** Only measure happiness with the opening keynote. 

## Learn why

Numbers tell you **what** is going on, not **why**. To learn why, proactively reach out to your users or customers and ask them.

- Schedule five 30-minute conversations each and every month. During these interviews, consider asking these questions:
  - What are you mostly happy with about the product? Why?
  - What aspects of the product frustrate you the most? Tell me more. 
  - If you had 15 minutes with our CEO, what would you tell her?
- Each month, gather all your notes into top themes and create a shared understanding of those themes among team members.

## Simplify

- Ask yourself what is the minimum amount of effort you could put into measuring happiness to get the maximum amount of learning of how valuable measurements are. When you have an answer, run with it and see what happens. 
- You will know happiness measurements are valuable when team members:
  - Ask questions about results.
  - Make requests for more detailed measurements.
  - Primarily use results when discussing or making decisions.
- Only after measurements are proven valuable, then can you move forward with the big honk vendor, survey, or measurement volume. 



# Building a happiness measurement system

- Implement a happiness measurement system to **define**, **collect**, and **track** attitudinal and experiential metrics consistently across products and services over time to elevate product quality and user satisfaction.
- Allows for the development of tools that allow teams in your organization to learn about customer happiness across different channels.
- Provides ways to measure happiness that are familiar to users and make sense to internal stakeholders.
- Generates **trustworthy data** with high validity and reliability.
- Provides happiness data that leads to **action** and **positive change**.
- A mature happiness measurement practice might include several channels for measuring happiness that belong to one system (e.g., physical happiness meter used in physical spaces, an app used in events, and a survey sent over email).